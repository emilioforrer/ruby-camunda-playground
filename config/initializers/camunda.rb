Camunda::Workflow.configure do |config|
  config.engine_route_prefix = 'engine-rest'
  config.engine_url = "http://camunda-engine:8080"
end