#!/bin/bash
set -e;

./bin/engines all bundle install;
bundle install;
bundle exec rails db:create;
bundle exec rails db:migrate;
bundle exec rails db:migrate RAILS_ENV=test;
./bin/engines ips bundle exec rails db:create;
./bin/engines ips bundle exec rails db:migrate RAILS_ENV=test;
./bin/engines ips bundle exec rubocop --fail-level F
./bin/engines ips bundle exec rails test;
