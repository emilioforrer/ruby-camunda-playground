#!/bin/bash

set -e; # This will cause the script to exit on first error
source /scripts/.bashrc;

source ci-cd/scripts/functions.inc.bash;

vault_login;

export CI_ENV=$(branch_env ${CI_COMMIT_REF_NAME});

export VERSION=$(branch_semver ${CI_COMMIT_REF_NAME} ${CI_COMMIT_SHA:0:12});

export DOCKER_REGISTRY_URL=${CI_REGISTRY}

export DOCKER_REGISTRY_USERNAME=${CI_REGISTRY_USER};

export DOCKER_REGISTRY_PASSWORD=${CI_REGISTRY_PASSWORD};

export REPO_REGISTRY_URL=registry.gitlab.com/agile-mindset/image-processing-system-api


println blue "Running Tagging docker image";

docker load --input ./.docker-cache/tmp-image.docker;

docker tag \
        ips-api:latest \
        ${REPO_REGISTRY_URL}:${VERSION};

println blue "Logging to the registry";

docker login ${DOCKER_REGISTRY_URL} -u ${DOCKER_REGISTRY_USERNAME} -p ${DOCKER_REGISTRY_PASSWORD};

println blue "Pushing image to docker registry";

docker push ${REPO_REGISTRY_URL}:${VERSION};

println blue "Image Pushed to docker registry";

println green "Step finished successfully!";