#!/bin/bash

set -e; # This will cause the script to exit on first error
source /scripts/.bashrc;

source ci-cd/scripts/functions.inc.bash;

vault_login;

export CI_ENV=$(branch_env ${CI_COMMIT_REF_NAME});

export WORKSPACE=/home/developer/workspace;

sudo chown -R developer $WORKSPACE;


export VERSION=$(branch_semver ${CI_COMMIT_REF_NAME} ${CI_COMMIT_SHA:0:12});

vault_get_k8s_chart_values > ./ci-cd/k8s/app-chart/values-deploy.yaml;

vault_get_k8s_config  > /tmp/kubeconfig;


export KUBECONFIG=/tmp/kubeconfig;

println blue "Upgrading Helm chart";

sed -i "/appVersion/c\\appVersion: ${VERSION}" ./ci-cd/k8s/app-chart/Chart.yaml;

helm upgrade --install image-processing-system-api  \
                -f ./ci-cd/k8s/app-chart/values-deploy.yaml \
                -n $(get_namespace ${CI_COMMIT_REF_NAME}) \
                ./ci-cd/k8s/app-chart --wait;

println green "Step finished successfully!";