#!/bin/bash

set -e; # This will cause the script to exit on first error
source /scripts/.bashrc;

source ci-cd/scripts/functions.inc.bash;

export CI_ENV=$(branch_env ${CI_COMMIT_REF_NAME});

println blue "Logging to the vault";

vault_login;

println blue "Getting .env file";

vault_get_ci_dot_env > .env;

docker-compose build

docker-compose run --rm web ./ci-cd/docker/ci-cmd.sh;

docker save --output .docker-cache/tmp-image.docker ips-api;

println green "Step finished successfully!";