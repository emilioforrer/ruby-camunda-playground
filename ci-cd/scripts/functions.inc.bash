#!/bin/bash

function vault_login() {
    BW_SESSION="";
    bw logout 2> /dev/null || true;
    export BW_SESSION=$(bw login ${VAULT_USER} ${VAULT_PASSWORD} --raw 2> /dev/null);
    # bw login --apikey --raw 2> /dev/null;
    # export BW_SESSION=$(bw unlock ${BW_PASSWORD} --raw 2> /dev/null);
}

function vault_get_ci_dot_env() {
   bw list items --raw --search "ips-api-env-${CI_ENV}" | jq -r '.[0].notes'
}

function vault_get_k8s_chart_values() {
   bw list items --raw --search "ips-api-chart-values-yaml-${CI_ENV}" | jq -r '.[0].notes'
}

function vault_get_k8s_config() {
#    bw list items --raw --search "kubeconfig-${CI_ENV}" | jq -r '.[0].notes'
  echo "${DEPLOYER_KUBECONFIG}";
}

function branch_env() {
    local BRANCH=$1
    local CI_ENV=$1
    case ${BRANCH} in
    "develop")
        CI_ENV=development
        ;;
    "staging")
        CI_ENV=staging
        ;;
    "master")
         CI_ENV=production
        ;;
    *)
        CI_ENV=development
        ;;
    esac
    echo "${CI_ENV}"
} 

function branch_semver() {
    local TAG=$(git tag -l | tail -n 1)
    [[ -z "${TAG}" ]]  && TAG="v0.1.0"
    local BRANCH=$1
    local SUFFIX=$2
    case ${BRANCH} in
    "develop")
        TAG=$(semver inc minor ${TAG})
        TAG=$(semver set prerelease  ${TAG} "alpha.${SUFFIX}")
        ;;
    "staging")
        TAG=$(semver inc minor ${TAG})
        TAG=$(semver set prerelease  ${TAG} "beta.${SUFFIX}")
        ;;
    "master")
        TAG=$(semver inc minor ${TAG})
        ;;
    esac
    echo "${TAG}"
}

function get_namespace() {
    local BRANCH=$1
    local NAMESPACE=$1
    case ${BRANCH} in
    "development")
        NAMESPACE=development
        ;;
    "staging")
        NAMESPACE=staging
        ;;
    "master")
        NAMESPACE=production
        ;;
    *)
        NAMESPACE=development
        ;;
    esac
    echo "${NAMESPACE}"
} 