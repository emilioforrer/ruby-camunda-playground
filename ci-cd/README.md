# Guide to setup the project

## Create rails app

```
docker run --rm -it -w /workspace -v $(pwd):/workspace -u 1000 ruby:2.7.2 bash
gem install rails
rails new  --database postgresql --api --skip-sprockets --skip-spring --skip-javascript  ips_api_app
```

## Copy template files to the root project

```bash
cp -RT ci-cd/template/
cp config/database.yml.example config/database.yml
cp .env.example .env
```

## Generate engine

```
mkdir engines
cd engines

```

## Add to `config/application.rb

```ruby
    # Custom config
    
    if ENV["RAILS_LOG_TO_STDOUT"].present?
      # https://blog.eq8.eu/til/ruby-logs-and-puts-not-shown-in-docker-container-logs.html
      config.logger = Logger.new('/proc/1/fd/1')
    end
    config.hosts << "localhost"
    config.hosts << "api-dev.mydomain.com"
    config.hosts << "api-staging.mydomain.com"
    config.hosts << "api.mydomain.com"
```