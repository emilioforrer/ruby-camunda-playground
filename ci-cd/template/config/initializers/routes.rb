# Setup default URL options
Rails.application.routes.default_url_options[:host] = ENV.fetch('RAILS_HOST', 'localhost')
Rails.application.routes.default_url_options[:protocol] = ENV.fetch('RAILS_PROTOCOL', 'http')
Rails.application.config.action_controller.default_url_options = Rails.application.routes.default_url_options