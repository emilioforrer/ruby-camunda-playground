redis_config = { url: "redis://#{ENV['REDIS_URL']}", network_timeout: 10 }

redis_config[:password] = "#{ENV['REDIS_PASSWORD']}=" if ENV['REDIS_PASSWORD'].present?

::Sidekiq.configure_client do |config|
  config.redis = redis_config
end

::Sidekiq.configure_server do |config|
  config.redis = redis_config
end