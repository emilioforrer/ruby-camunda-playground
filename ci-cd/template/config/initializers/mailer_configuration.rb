mailer = ActionMailer::Base
mailer.delivery_method = :smtp
mailer.perform_deliveries = ENV.fetch('MAILER_PERFORM_DELIVERIES', true)
mailer.raise_delivery_errors = true

mailer.default_url_options = {
  host: ENV.fetch('RAILS_HOST', 'localhost'),
  port: ENV.fetch('RAILS_PORT', 3000),
}

mailer.default_options = {
  from: "MyDomain <#{ENV.fetch('MAILER_FROM', 'no-reply@mydomain.com')}>"
}

mailer.smtp_settings = {
  address: ENV.fetch('MAILER_ADDRESS', 'smtp.sendgrid.net'),
  port: ENV.fetch('MAILER_PORT', 587),
  user_name: ENV.fetch('MAILER_USER_NAME', ''),
  password: ENV.fetch('MAILER_PASSWORD', ''),
  domain: ENV.fetch('MAILER_DOMAIN', ''),
  authentication: ENV.fetch('MAILER_AUTHENTICATION', 'plain'),
  enable_starttls_auto: ENV.fetch('MAILER_TTLS', true)
}