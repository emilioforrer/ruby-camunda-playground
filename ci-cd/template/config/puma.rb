application_path = File.expand_path('..', __dir__)
threads_count = ENV.fetch("RAILS_MAX_THREADS", 5)
threads threads_count, threads_count
preload_app!
rackup      DefaultRackup
port        ENV['PORT'] || 3000
environment ENV.fetch("RAILS_ENV") { "development" }
# daemonize true
pidfile "#{application_path}/tmp/pids/puma.pid"
state_path "#{application_path}/tmp/pids/puma.state"
stdout_redirect "#{application_path}/log/puma.stdout.log", "#{application_path}/log/puma.stderr.log"
# bind "unix://#{application_path}/tmp/sockets/puma.sock"
plugin :tmp_restart
workers ENV.fetch('RAILS_WEB_CONCURRENCY', 2)
on_worker_boot do
  # worker specific setup
  ActiveSupport.on_load(:active_record) do
    config = ActiveRecord::Base.configurations[Rails.env] ||
             Rails.application.config.database_configuration[Rails.env]
    config['pool'] = ENV['RAILS_MAX_THREADS'] || 16
    ActiveRecord::Base.establish_connection(config)
  end
end
