# Install KIND

```bash
curl -Lo ./kind https://github.com/kubernetes-sigs/kind/releases/download/v0.7.0/kind-$(uname)-amd64
chmod +x ./kind
sudo mv kind /usr/local/bin/
```

## Create Cluster


```bash
kind create cluster --name dev
```

## Cluster info

```
kubectl cluster-info --context kind-dev
```

## Rename

* Update the chart name in `Chart.yaml` files
* Update app name in `values.yaml` files
