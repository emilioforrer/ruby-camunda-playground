class WorkflowExternalServiceProcess::WorkflowExternalService < CamundaJob
  def bpmn_perform(variables)
    Rails.logger.info "\n\nEXECUTING => ExecuteAgeValidationTask with valiables #{variables.inspect}  \n\n"
    # start_response = Camunda::ProcessDefinition.start_by_key 'CamundaWorkflow', variables: { x: 'abcd' }, businessKey: 'WorkflowBusinessKey'
    # Camunda::Task.find_by_business_key_and_task_definition_key!("90aacc98-7277-11ec-9e1d-0242c0a82002", "WorkflowExternalService").complete!
    # A hash returned will become variables in the Camunda BPMN process instance
    { foo: 'bar', foo2: { json: "str" }, array_var: ["str"] }
    # WorkflowExternalServiceProcess::WorkflowExternalService
    # Camunda::Poller.fetch_and_queue %w[ExecuteAgeValidationTask]
  end
end


